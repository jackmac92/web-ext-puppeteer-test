#!/usr/bin/env bash
set -euo pipefail

if [ -n "${DEBUG:-""}" ]; then
    set -x
fi

cd /app/run

if ! [[ -f "$EXTENSION_PATH/manifest.json" ]]; then
    echo "NO EXTENSION PRESENT (couldn't find manifest), exiting because chrome will just hang"
    ls "$EXTENSION_PATH"
    exit 1
fi

mkdir -p "$TESTS_PATH"
cat "$EXTENSION_PATH/manifest.json" | python3 -c '
import json
import sys
c = json.load(sys.stdin)
assert len(c.get("key", "")) > 10, "Manifest key appears to be missing, which means you get a randomly generated extension id"
'

if [ -n "${INSERT_TEST_FLAG:-""}" ]; then
    for entry in $(cat "$EXTENSION_PATH/manifest.json" | python3 extract-manifest-entrypoints.py); do
        (
            echo "globalThis.WEB_EXT_TEST_ENV = true;"
            cat $entry
        ) | sponge $entry
    done
fi

if [ -z $(npm exec jest -- --listTests) ]; then
    echo "No tests provided!! using default tests"
    mv defaultTests/* "$TESTS_PATH"
elif [ -n ${WITH_DEFAULT_TESTS:-""} ]; then
    echo "Including default tests"
    mv defaultTests/* "$TESTS_PATH"
fi

Xvfb $DISPLAY -auth /tmp/xvfb.auth -ac -screen 0 1920x1080x24 -listen tcp &
xvfbpid=$!

mkdir -p "$TEST_RUN_ARTIFACTS_PATH"
recordingpath="$TEST_RUN_ARTIFACTS_PATH/video_out/$(date +%s).mp4"
mkdir -p $(dirname "$recordingpath")
touch "$recordingpath"

ffmpglogdir="$TEST_RUN_ARTIFACTS_PATH/ffmpeg_logs"

mkdir -p "$ffmpglogdir"

ffcomm=$(mktemp)
echo >"$ffcomm"
tail -f "$ffcomm" | ffmpeg -y -f x11grab -i 127.0.0.1$DISPLAY -video_size 1920x1080 -codec:v libx264 -r 12 -vf "drawtext=fontfile=/usr/share/fonts/opentype/ipafont-gothic/ipag.ttf \
        :text=Loading \
        :fontcolor=white \
        :fontsize=24 \
        :box=1 \
        :boxcolor=black@0.5 \
        :x=0 \
        :y=0, \
        format=yuv420p" "$recordingpath" >"$ffmpglogdir/out.log" 2>"$ffmpglogdir/err.log" &

ffpid=$!

export TEST_RUN_ARTIFACTS_PATH
set -x
npm exec jest -- --verbose
exit_status=$?
echo "q" >>"$ffcomm"
sleep 2
echo "q" >>"$ffcomm"
sleep 2
wait "$ffpid"
kill -9 "$xvfbpid"
exit "$exit_status"
