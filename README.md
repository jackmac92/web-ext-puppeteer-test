# Web Ext Puppeteer Integration Test Image

```bash
docker run --rm \
    -v /path/to/your/e2e-tests/:/app/run/tests/ \
    -v /path/to/extension-build/:/web-extension/ \
    ci-run-puppeteer-tests
```

<!-- TODO document VNC setup -->

### Gitlab example jobs

By invoking `docker run` with a mounted volume
(The before script is used to pull the most recent build artifact from the master pipeline)

```yaml
test build on chrome:
  stage: test-build
  interruptible: true
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - >
      docker run --rm
      -w /app
      -v $(pwd):/app
      curlimages/curl:7.78.0
      --location
      --output artifacts.zip
      --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/master/download?job=build"
    - rm -rf web-ext-dist
    - unzip artifacts.zip
  script:
    - if ! [ "$EXT_TARGET" == "chrome" ]; then exit 0; fi
    - ls web-ext-dist
    - cat web-ext-dist/manifest.json
    - docker run -e GOOGLE_WEBSTORE_APP_ID -v $PWD/$WEB_EXT_CI_INTEGRATION_TESTS_PATH:/app/run/tests -v $PWD/web-ext-dist/:/web-extension -v $PWD/test-evidence/:/app/run/outbox/ jackzzz92/web-ext-puppeteer-test ci-run-puppeteer-tests

  cache: {}
  artifacts:
    when: always
    paths:
      - ./web-ext-dist
      - ./test-evidence
```

Or you can simply specify the docker image as the `image` for the job, (you must ensure your extension is available at $EXTENSION_PATH, but you can change that path)

```yaml
generic test build on chrome:
  stage: test-build
  interruptible: true
  image: jackzzz92/web-ext-puppeteer-test
  before_script:
    - >
      curl
      --location
      --output artifacts.zip
      --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/master/download?job=build"
    - rm -rf web-ext-dist
    - unzip artifacts.zip
  script:
    - ci-run-puppeteer-tests
  cache: {}
  variables:
    TEST_RUN_ARTIFACTS_PATH: $CI_PROJECT_DIR/test-evidence
    EXTENSION_PATH: $CI_PROJECT_DIR/web-ext-dist
    WITH_DEFAULT_TESTS: 1
  artifacts:
    when: always
    paths:
      - ./web-ext-dist
      - ./test-evidence
  except:
    variables:
      - $WEB_EXT_CI_INTEGRATION_TESTS_PATH
  only:
    changes:
      - src/**/*
```
