// const merge = require("merge");
// const ts_preset = require("ts-jest/jest-preset");
// const puppeteer_preset = require("jest-puppeteer/jest-preset");
const path = require("path");

const testsPath = path.relative(process.cwd(), process.env.TESTS_PATH);

module.exports = {
  verbose: true,
  preset: "jest-puppeteer",
  testRegex: `${testsPath}/.*\.(j|t)sx?$`,
  setupFilesAfterEnv: ["./jest-puppeteer-setup.js"],
  globalSetup: "jest-environment-puppeteer/setup",
  globalTeardown: "jest-environment-puppeteer/teardown",
  testEnvironment: "jest-environment-puppeteer",
  testPathIgnorePatterns: ["/node_modules/"],
  moduleDirectories: ["node_modules"],
  moduleFileExtensions: ["js", "ts"],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.jsx?$": "babel-jest",
  },
};
