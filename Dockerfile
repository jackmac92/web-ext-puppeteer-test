FROM node:20

RUN apt-get update -y
RUN apt-get install -y --no-install-recommends \
	python3 \
	moreutils \
	git \
	xauth \
	ffmpeg \
	libatk1.0-0 \
	libatk-bridge2.0-0 \
	xvfb \
	gconf-service \
	libasound2 \
	libc6 \
	libcairo2 \
	libcups2 \
	libdbus-1-3 \
	libexpat1 \
	libfontconfig1 \
	libgcc1 \
	libgconf-2-4 \
	libgdk-pixbuf2.0-0 \
	libglib2.0-0 \
	libgtk-3-0 \
	libnspr4 \
	libpango-1.0-0 \
	libpangocairo-1.0-0 \
	libstdc++6 \
	libx11-6 \
	libx11-xcb1 \
	libxcb1 \
	libxcomposite1 \
	libxcursor1 \
	libxdamage1 \
	libxext6 \
	libxfixes3 \
	libxi6 \
	libxrandr2 \
	libxrender1 \
	libxss1 \
	libxtst6 \
	ca-certificates \
	fonts-liberation \
	libappindicator1 \
	libnss3 \
	lsb-release \
	xdg-utils \
	wget \
	&& apt-get clean autoclean \
	&& apt-get autoremove --yes \
	&& rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN npm install -g npm@latest
RUN ln -sf /var/lib/dbus/machine-id /etc/machine-id


ENV NODE_ENV=development

WORKDIR /app/run

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

RUN chmod +x ./boot-test.sh
RUN ln -s /app/run/boot-test.sh /usr/bin/ci-run-puppeteer-tests
ENV DISPLAY=:99
# ENV IS_DOCKER=1
# ENV WD_TEST=1
ENV EXTENSION_PATH=/web-extension
ENV TESTS_PATH=/app/run/tests/
ENV PUPPETEER_TEST_DUMPIO=1
ENV PUPPETEER_TEST_AUTOOPEN_DEVTOOLS=1
ENV TEST_RUN_ARTIFACTS_PATH=/app/run/outbox/
ENTRYPOINT ["bash"]
