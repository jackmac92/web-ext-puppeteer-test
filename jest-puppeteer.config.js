const path = require("path");
const puppeteer = require("puppeteer");

const args = puppeteer
  .defaultArgs()
  .filter(
    (arg) =>
      !["--disable-extensions", "--headless"].includes(
        String(arg).toLowerCase()
      )
  );

if (!process.env.EXTENSION_PATH) {
  throw new Error("Need to know the extenion path");
}

args.push(`--load-extension=${path.resolve(process.env.EXTENSION_PATH)}`);

// for running in container as root
args.push("--no-sandbox");
args.push("--disable-setuid-sandbox");

module.exports = {
  launch: {
    ignoreDefaultArgs: true,
    args,
    dumpio: Boolean(process.env.PUPPETEER_TEST_DUMPIO),
    devtools: Boolean(process.env.PUPPETEER_TEST_AUTOOPEN_DEVTOOLS),
  },
};
