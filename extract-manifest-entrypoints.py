import sys
import json

manifest = json.load(sys.stdin)

jsEntrypoints = []

# background script
if manifest["background"].get("page"):
    # TODO find script tags for js?
    pass
else:
    jsEntrypoints += manifest["background"].get("scripts")

# content script entrypoints
for z in filter(lambda x: x, map(lambda x: x.get("js"), manifest.get("content_scripts", []))):
    jsEntrypoints += z

[print(e) for e in jsEntrypoints]
