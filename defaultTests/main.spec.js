const fs = require("fs");
const mkdirp = require("mkdirp");
const path = require("path");
const { toJSON } = require("flatted");

async function JSONToFile(obj, filename) {
  const baseDir = path.dirname(process.env.TEST_RUN_ARTIFACTS_PATH);
  await mkdirp(baseDir);
  const filePath = path.join(baseDir, `${filename}.json`);
  const jsonOutput = toJSON(obj);
  return new Promise((resolve) => {
    fs.writeFile(filePath, JSON.stringify(jsonOutput), resolve);
  });
}

describe("control test", () => {
  it("should be able to hit google", async () => {
    await page.goto("https://google.com");
    expect(await page.title()).toMatch("Google");
  });
});

describe("extension 'reflective' testing", () => {
  beforeAll(() => {
    if (!process.env.GOOGLE_WEBSTORE_APP_ID) {
      throw new Error("You must provide GOOGLE_WEBSTORE_APP_ID as an env var");
    }
  });
  describe("manifest testing", () => {
    it("should be able to read manifest", async () => {
      const manifestUrl = `chrome-extension://${process.env.GOOGLE_WEBSTORE_APP_ID}/manifest.json`;
      let pageError = false;
      const resp = await page.goto(manifestUrl);
      expect(resp.ok()).toBe(true);
    });
  });
  describe("background page", () => {
    it("should not have a bunch of errors", () => {
      const bgPage = `chrome-extension://${process.env.GOOGLE_WEBSTORE_APP_ID}/_generated_background_page.html`;
      let pageErrorCount = 0;
      let consoleErrorCount = 0;
      const errorCache = { page: [], console: [] };
      page.on("console", async (msg) =>
        console[msg._type](
          ...(await Promise.all(msg.args().map((arg) => arg.jsonValue()))),
        ),
      );
      page.on("pageerror", (err) => {
        // pageErrorCount += 1;
        errorCache.page.push(toJSON(err));
      });
      page.on("console", (consoleMsg) => {
        if (consoleMsg.type() === "error") {
          errorCache.console.push(toJSON(consoleMsg));
          const { url: errorFile, lineNumber: errorLineNo } =
            consoleMsg.location();
          // should check url is from this extension
          if (errorLineNo) {
            consoleErrorCount += 1;
            console.log(
              "Console error: from " +
              errorFile.toString() +
              " at " +
              errorLineNo.toString() +
              ": " +
              JSON.stringify(consoleMsg.text())
            );
          }
        }
      });
      return page
        .goto(bgPage)
        .then(
          () =>
            new Promise((resolve) => {
              setTimeout(resolve, 1000);
            }),
        )
        .then(() => {
          JSONToFile(errorCache, "testConsoleErrors.dump");
          const overallErrorCount = pageErrorCount + consoleErrorCount;
          console.log(`Found ${overallErrorCount} errors`);
          expect(overallErrorCount).toBeLessThan(2);
        });
    });
  });
});
